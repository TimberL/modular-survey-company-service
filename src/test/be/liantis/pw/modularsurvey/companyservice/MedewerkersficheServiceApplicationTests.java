package be.liantis.pw.modularsurvey.companyservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ModularSurveyCompanyServiceApplication.class)
@ActiveProfiles({"local", "unittest"})
public class MedewerkersficheServiceApplicationTests {

    @Test
    public void contextLoads() {
    }

}
